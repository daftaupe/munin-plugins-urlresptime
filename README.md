# munin-plugins-urlresptime
Use [cURL](https://curl.haxx.se/) to monitor an URL response times through [Munin](http://munin-monitoring.org) 

## Requirements
* [cURL](https://curl.haxx.se/)

## Available plugin
~~~bash
urlresptime # graph response times from a specific URL
~~~

## Installation
Like all munin plugins, you have to create a symbolic link in your Munin plugin directory (usually it is /etc/munin/plugins).
For example suppose you have downloaded the plugins in /usr/share/munin/plugins (with the default ones provided by Munin) you should run the following commands.

~~~bash
# installation of the plugin
# cd /usr/share/munin/plugins && git clone https://github.com/daftaupe/munin-plugins-urlresptime.git
# cd /etc/munin/plugins && ln -s /usr/share/munin/plugins/munin-plugins-urlresptime/urlresptime
~~~

## Configuration
Create /etc/munin/plugin-conf.d/urlresptime
Modify the following to fit the URL you want to monitor.

~~~bash
# URLresptime
[urlresptime]

env.URL http://www.fqdn.com
~~~

Restart your munin-node daemon.

## Preview
![urlresptime](https://cloud.githubusercontent.com/assets/22810624/23546498/1ce63a0c-0000-11e7-8030-6560d05369e1.png)
